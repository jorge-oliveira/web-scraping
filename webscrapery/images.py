from bs4 import BeautifulSoup
import requests
from PIL import Image
from io import BytesIO

search = input("Search for: ")
params = {"q": search}
r = requests.get("http://bing.com/images/search", params=params)

# get the response content and remove the warnings
soup = BeautifulSoup(r.text, "html.parser")

links = soup.find_all("a", {"class": "thumb"})

for item in links:

    img_obj = requests.get(item.attrs["href"])
    print("Getting:", item.attrs["href"])
    title = item.attrs["href"].split("/")[-1]
    img = Image.open(BytesIO(img_obj.content))
    img.save("/scraped_images/" + title, img.format)
