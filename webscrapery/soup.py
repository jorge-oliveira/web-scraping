from bs4 import BeautifulSoup
import requests

search = input("Enter search term: ")
params = {"q": search}
r = requests.get("https://www.bing.com/search", params=params)

sop = BeautifulSoup(r.text)
print(sop.prettify())
