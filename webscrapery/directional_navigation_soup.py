from bs4 import BeautifulSoup
import requests

search = input("Search for: ")
params = {"q": search}
r = requests.get("https://www.bing.com/search", params=params)

# gets the html content from the page without the warning (html.parser)
soup = BeautifulSoup(r.text, "html.parser")

# find the results matches the attribute
results = soup.find("ol", {"id": "b_results"})

# creating a list of all items from matching class
links = results.findAll("li", {"class": "b_algo"})

for item in links:

    # get the text from the element
    item_text = item.find("a").text
    item_ref = item.find("a").attrs["href"]

    if item_text and item_ref:
        print(item_text)
        print(item_ref)
        print("Parent: ", item.find("a").parent)

        # Compile a list of all childrens
        children = item.children
        for child in children:
            print("child:", child)

        # Find siblings
        sibling = item.find("h2")
        print("Next Sibling of the h2: ", sibling.next_sibling)
